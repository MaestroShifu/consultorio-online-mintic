import { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('drugs', table => {
        table.increments('ID').notNullable().primary();
        table.string('barcode').unique();
        table.string('name').notNullable();
        table.string('factory').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('drugs');
}
