import { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('profiles', table => {
        table.string('DNI').unique().notNullable().primary();
        table.string('password').notNullable();
        table.string('name').notNullable();
        table.string('lastName').notNullable();
        table.enu('type', ['admin' , 'doctor', 'patient'], { useNative: true, enumName: 'type_products' }).defaultTo('patient');
        table.date('birthday').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('profiles');
}
