import { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('list_drugs', table => {
        table.integer('meetingID').unsigned();
        table.integer('drugID').unsigned();
        table.string('description').notNullable();

        // Key foreign
        table.foreign('meetingID').references('ID').inTable('meetings');
        table.foreign('drugID').references('ID').inTable('drugs');
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('list_drugs');
}
