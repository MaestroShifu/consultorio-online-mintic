import { Knex } from 'knex';


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('meetings', table => {
        table.increments('ID').notNullable().primary();
        table.string('patientDNI');
        table.string('doctorDNI').notNullable();
        table.string('address');
        table.dateTime('date').notNullable();
        table.boolean('isCanceled').notNullable().defaultTo(false);

        // Key foreign
        table.foreign('patientDNI').references('DNI').inTable('profiles');
        table.foreign('doctorDNI').references('DNI').inTable('profiles');
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('meetings');
}
