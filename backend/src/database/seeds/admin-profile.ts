import { Knex } from 'knex';
import { TypeProfile } from '../models/profile';
import { generateHash } from '../../lib/bycript';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('profiles').del();

    // Inserts seed entries
    await knex("profiles").insert([
        { 
            DNI: '0000',
            password: generateHash('root'),
            name: 'admin',
            lastName: 'root',
            type: TypeProfile.admin,
            birthday: '1997-08-19'
        },
    ]);
};
