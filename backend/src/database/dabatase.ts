import knex from 'knex';
import { Model } from 'objection';
import { getConfig } from '../config';

const config = getConfig();

export const startConnectionDB = () => {
    const db = knex({ 
        client: 'mysql2',
        connection: {
          host: config.HOST_DATABASE,
          port: config.PORT_DATABASE,
          user: config.USER_DATABASE,
          password: config.PASSWORD_DATABASE,
          database: config.NAME_DATABASE
        },
        debug: config.ENV === 'dev', // Imprimir las sentencias SQL en consola
        pool: { // Sirve para determinar una cantidad maxima de conecciones a la base de datos
          min: 2,
          max: 10
        },
        acquireConnectionTimeout: 10000, // Cuanto se debe esperar para generar un error al no conectarse a la db
    });
    Model.knex(db); // Agregar libreria objection
};