
import { Model } from 'objection';

export type Meeting = {
    ID: number;
    patientDNI?: string;
    doctorDNI: string;
    address?: string;
    date: Date;
    isCanceled: boolean;
}

export class MeetingModel extends Model implements Meeting { 
    static tableName = 'meetings';
    
    static idColumn= 'ID';

    public ID!: number;
    public patientDNI?: string;
    public doctorDNI!: string;
    public address?: string;
    public date!: Date;
    public isCanceled!: boolean; 
}
