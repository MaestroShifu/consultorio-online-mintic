import { Model } from 'objection';

export type Profile = {
    DNI: string;
    password: string;
    name: string;
    lastName: string;
    type: TypeProfile;
    birthday: Date; 
}

export enum TypeProfile {
    'admin' = 'admin',
    'doctor' = 'doctor',
    'patient' = 'patient'
};

export class ProfileModel extends Model implements Profile {
    static tableName= 'profiles';
    static idColumn= 'DNI';

    public DNI!: string;
    public password!: string;
    public name!: string;
    public lastName! : string;
    public type!: TypeProfile;
    public birthday!: Date; 
}