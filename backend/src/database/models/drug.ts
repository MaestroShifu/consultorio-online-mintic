import { Model } from 'objection';

export type Drug = {
    ID: number;
    barcode?: string;
    name: string;
    factory: string;
}

export class DrugModel extends Model implements Drug { 
    static tableName = 'drugs';

    static idColumn = 'ID';

    public ID!: number;
    public barcode?: string;
    public name!: string;
    public factory!: string;
}
