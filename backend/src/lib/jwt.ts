import jwt, { JwtPayload, Jwt } from 'jsonwebtoken';
import { getConfig } from '../config';

export type DataWithToken<T = unknown> = T & { token: string; };

const { JWT_SECRETS } = getConfig();

export const generateToken = (args: string | Buffer | object): string => {
  return jwt.sign(args, JWT_SECRETS);
};

export const verifyToken = (token: string): Jwt | JwtPayload | string => {
  return jwt.verify(token, JWT_SECRETS);
};
