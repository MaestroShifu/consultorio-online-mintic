import bcrypt from 'bcryptjs';
import { getConfig } from '../config';

const { SALT } = getConfig();

export const generateHash = (text: string): string => {
  const salt = bcrypt.genSaltSync(SALT);
  return bcrypt.hashSync(text, salt);
};

export const compareHash = (hash: string, text: string): boolean => {
  return bcrypt.compareSync(text, hash);
};
