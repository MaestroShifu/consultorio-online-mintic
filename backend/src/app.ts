import { meetingController } from './controllers/meeting-controller';
import express from 'express';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import { profileController } from './controllers/profile-controller';
import { drugController } from './controllers/drug-controller';
import { startConnectionDB } from './database/dabatase';


export const app = express();

// Set security Http headers
app.use(helmet());
// Parse json request body
app.use(bodyParser.json());
// parse urlencoded request body
app.use(bodyParser.urlencoded({extended: false}));
// gzip compression
app.use(compression());
// Enable cors
app.use(cors());

// Controllers
profileController(app);
drugController(app);
meetingController(app);

// Start DB
startConnectionDB();
