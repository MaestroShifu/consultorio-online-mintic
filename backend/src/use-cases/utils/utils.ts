import { Profile } from './../../database/models/profile';
import { generateToken } from './../../lib/jwt';


export type ResponseAuth = Omit<CreateProfile, 'password'> & {
  token: string;
};

export const createToken = (profile: Profile): string => {
  return generateToken({
    DNI: profile.DNI,
    name: profile.name
  });
};
