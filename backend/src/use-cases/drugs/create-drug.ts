import { Drug, DrugModel } from "../../database/models/drug";

export const createDrug = async (args: Omit<Drug, 'ID'>): Promise<DrugModel> => {
    const drug = await DrugModel.query().insert({ ...args });
    return drug;
}