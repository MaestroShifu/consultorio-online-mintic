import { Drug, DrugModel } from "../../database/models/drug";

export const listDrugs = async (): Promise<Drug[]> => {
    const drugs = await DrugModel.query()
    return drugs;
}