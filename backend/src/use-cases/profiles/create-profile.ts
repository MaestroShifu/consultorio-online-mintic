import { ProfileModel, Profile } from "../../database/models/profile";
import { generateHash } from "../../lib/bycript";
import { DataWithToken, generateToken } from "../../lib/jwt";

type CreateProfile = Omit<Profile, 'password'>;

export const createProfile = async (args: Profile): Promise<DataWithToken<CreateProfile>> => {
    const profile = await ProfileModel.query().findById(args.DNI);
    if(profile) {
        throw new Error("DNI already exists");
    }
    const password = generateHash(args.password);
    const newProfile = await ProfileModel.query().insert({ 
        ...args,
        password,
        name: args.name.toLowerCase(),
        lastName: args.lastName.toLowerCase()
    });
    const token = generateToken({
        DNI: newProfile.DNI,
        type: newProfile.type
    });
    return {
        DNI: newProfile.DNI,
        name: newProfile.name,
        lastName: newProfile.lastName,
        type: newProfile.type,
        birthday: newProfile.birthday,
        token
    };
}