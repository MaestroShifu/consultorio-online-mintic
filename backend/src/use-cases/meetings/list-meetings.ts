import { Meeting, MeetingModel } from "../../database/models/meeting";

export const listMeetings = async (id: string, isDoctor: boolean): Promise<Meeting[]> => {
    const field = isDoctor ? 'doctorDNI' : 'patientDNI';
    const meetings = await MeetingModel.query().where(field, id);
    return meetings;
}