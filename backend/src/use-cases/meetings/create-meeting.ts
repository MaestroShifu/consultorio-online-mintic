import { ProfileModel } from '../../database/models/profile';
import { Meeting, MeetingModel } from './../../database/models/meeting';

type CreateMeeting = Omit<Meeting, 'ID' | 'isCanceled'>; 

export const createMeeting= async (args: CreateMeeting): Promise<Meeting> => {
    const doctor = await ProfileModel.query().findById(args.doctorDNI);
    if (!doctor)
        throw new Error("Profile not found");
    if (doctor.type !== 'doctor')
        throw new Error("Doctor not found");
    const meeting = await MeetingModel.query().insert({...args});
    return {
        ...meeting
    };
}