import { Express, Request, Response } from 'express';
import { createDrug } from '../use-cases/drugs/create-drug';
import { listDrugs } from '../use-cases/drugs/list-drug';

export const drugController = (app: Express) => {
    app.get('/drugs', async (req: Request, res: Response) => {
        try {  
            const drugs = await listDrugs();
            return res.status(200).send({
                data: drugs
            });
        } catch (error: any) {
            return res.status(400).send({ msg: error.message });
        }
    });

    app.post('/drugs', async (req: Request, res: Response) => {
        try {  
            const { barcode, name, factory } = req.body;     
            if (!name || !factory) {
                throw new Error('Hacen falta datos en la peticion');                
            }
            const drug = await createDrug({
                barcode,
                name,
                factory
            });
            return res.status(201).send({
                data: drug
            });
        } catch (error: any) {
            return res.status(400).send({ msg: error.message });
        }
    });
}
