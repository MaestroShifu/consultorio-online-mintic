import { createMeeting } from './../use-cases/meetings/create-meeting';
import { Express, Request, Response } from 'express';
import { listMeetings } from '../use-cases/meetings/list-meetings';

export const meetingController = (app: Express) => {
    app.post('/meetings', async (req: Request, res: Response) => {
        try{
            const {
                doctorDNI,
                date,
                address,
            } = req.body;
            if(!doctorDNI)
                new Error('doctorDNI not found')
            if(!date)
                new Error('doctorDNI not found')

            const meeting = await createMeeting({
                doctorDNI,
                address,
                date             
            });
            res.status(201).send({
                data: meeting
            });   
        }catch (error: any){
            res.status(400).send({
                msg: error.message
            });
        }        
    });
    app.get('/meetings/doctor/:id', async (req: Request, res: Response) => {
        try{
            const { id } = req.params;
            const meetings = await listMeetings(id, true);
            res.status(201).send({
                data: meetings
            });   
        }catch (error: any){
            res.status(400).send({
                msg: error.message
            });
        }        
    });
    app.get('/meetings/patient/:id', async (req: Request, res: Response) => {
        try{
            const { id } = req.params;
            const meetings = await listMeetings(id, false);
            res.status(201).send({
                data: meetings
            });   
        }catch (error: any){
            res.status(400).send({
                msg: error.message
            });
        }        
    });
}
