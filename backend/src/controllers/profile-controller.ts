import { Express, Request, Response } from 'express';
import { Profile, TypeProfile } from '../database/models/profile';
import { createProfile } from '../use-cases/profiles/create-profile';

const validateCreate = (args: Omit<Profile, 'type'>): Omit<Profile, 'type'> => {
    const {
        DNI,
        password,
        name,
        lastName,
        birthday
    } = args;

    if(!DNI)
        throw new Error('DNI not found');        
    if(!password)
        throw new Error('password not found');        
    if(!name)
        throw new Error('name not found');        
    if(!lastName)
        throw new Error('lastName not found');        
    if(!birthday)
        throw new Error('birthday not found');        

    return {
        DNI,
        password,
        name,
        lastName,
        birthday
    }
}

export const profileController = (app: Express) => {
    app.post('/patients', async (req: Request, res: Response) => {
        try{
            const {
                DNI,
                password,
                name,
                lastName,
                birthday
            } = validateCreate(req.body);

            const profile = await createProfile({
                DNI,
                password,
                name,
                lastName,
                birthday,
                type: TypeProfile.patient
            });

            return res.status(201).send({
                data: profile
            });   
        } catch (error: any) {
            return res.status(400).send({ msg: error.message });
        }
    });

    app.post('/doctors', async (req: Request, res: Response) => {
        try{
            const {
                DNI,
                password,
                name,
                lastName,
                birthday
            } = validateCreate(req.body);

            const profile = await createProfile({
                DNI,
                password,
                name,
                lastName,
                birthday,
                type: TypeProfile.doctor
            });

            return res.status(201).send({
                data: profile
            });   
        } catch (error: any) {
            return res.status(400).send({ msg: error.message });
        }
    });
}
