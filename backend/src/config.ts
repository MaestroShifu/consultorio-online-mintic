import dotenv, { DotenvConfigOptions } from 'dotenv';
import path from 'path';

type Enviroment = 'dev' | 'prod';

export type Config = {
    ENV: Enviroment;
    PORT: number;
    HOST_DATABASE: string;
    PORT_DATABASE: number;
    USER_DATABASE: string;
    PASSWORD_DATABASE: string;
    NAME_DATABASE: string;
    SALT: number;
    JWT_SECRETS: string;
}

const URL_PATH = '../.env';

const loadFileEnv = (url: string) => {
    const configEnv: DotenvConfigOptions = {
      path: path.join(path.dirname(__filename), url)
    };
    
    dotenv.config(configEnv);
}

export const getConfig = (): Config => {
    const ENV: Enviroment = process.env.ENV_NODE as Enviroment || 'dev';
    if(ENV === 'dev') {
        loadFileEnv(URL_PATH);
    }
    return {
        ENV,
        PORT: +(process.env.PORT || 3000),
        HOST_DATABASE: process.env.HOST_DATABASE || '',
        PORT_DATABASE: +(process.env.PORT_DATABASE || 3306),
        USER_DATABASE: process.env.USER_DATABASE || '',
        PASSWORD_DATABASE: process.env.PASSWORD_DATABASE || '',
        NAME_DATABASE: process.env.NAME_DATABASE || '',
        SALT: +(process.env.SALT || 8),
        JWT_SECRETS: process.env.JWT_SECRETS || ''
    }
};
