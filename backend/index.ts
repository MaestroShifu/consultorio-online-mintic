import { app } from "./src/app";
import { getConfig } from "./src/config";

const config = getConfig();

app.listen(config.PORT, () => {
    console.log(`[server]: Server is running at https://localhost:${config.PORT}`);
});