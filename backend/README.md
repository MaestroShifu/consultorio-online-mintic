# Backend project (Consultorio online MinTic)

* Command build docker image
```bash
    docker build -t citas-app-backend .  
```

* Command run container docker bind
```bash
    docker run -it --rm --name citas-app-backend --net container_net --mount type=bind,source="$(pwd)"/,target=/app citas-app-backend sh
```